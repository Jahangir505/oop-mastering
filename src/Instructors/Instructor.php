<?php

namespace Jahangir\Instructors;

class Instructor
{
    public $image = null;
    public $name = null;
    public $designation = null;
    public $social_media = [];

    public function getAllInstructors()
    {
        return $instructors = [
            ['image'=>'../front/assets/img/instructors/instructor-1.jpg', 'name' => 'Jack Wilson', 'designation' => 'Developer', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ['image'=>'../front/assets/img/instructors/instructor-2.jpg', 'name' => 'Anna Richard', 'designation' => 'Travel Bloger', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ['image'=>'../front/assets/img/instructors/instructor-3.jpg', 'name' => 'Kathelen Monero', 'designation' => 'Designer', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ['image'=>'../front/assets/img/instructors/instructor-4.jpg', 'name' => 'Kristen Pala', 'designation' => 'U/X Design', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ['image'=>'../front/assets/img/instructors/instructor-5.jpg', 'name' => 'Jack Wilson', 'designation' => 'Developer', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ['image'=>'../front/assets/img/instructors/instructor-6.jpg', 'name' => 'Kathelen Monero', 'designation' => 'Developer', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ['image'=>'../front/assets/img/instructors/instructor-7.jpg', 'name' => 'Anna Richard', 'designation' => 'Travel Bloger', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ['image'=>'../front/assets/img/instructors/instructor-8.jpg', 'name' => 'Kristen Pala', 'designation' => 'Developer', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ['image'=>'../front/assets/img/instructors/instructor-9.jpg', 'name' => 'Jack Wilson', 'designation' => 'Developer', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ['image'=>'../front/assets/img/instructors/instructor-10.jpg', 'name' => 'Kathelen Monero', 'designation' => 'Developer', 'social_media' => [
                ['social_uri' => '#', 'icon'=> 'fab fa-facebook-f'],
                ['social_uri' => '#', 'icon'=> 'fab fa-twitter'],
                ['social_uri' => '#', 'icon' => 'fab fa-instagram'],
                ['social_uri' => '#', 'icon'=> 'fab fa-linkedin-in'],
            ] ],
            ];
    }
}
