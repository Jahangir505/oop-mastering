<?php

namespace Jahangir\AppFeature;

class AppFeature
{
    public $icon = null;
    public $title = null;
    public $content = null;

    public function getFeatures()
    {
        return $features = [
            ['icon' => '../front/assets/img/IconImg/bookmark.png', 'title' => 'Bookmark Fav Courses', 'content' => 'Getting The Necessary Clarity About The Current State To Help You Improve Your Game.'],
            ['icon' => '../front/assets/img/IconImg/chat.png', 'title' => 'Live Chats', 'content' => 'Getting The Necessary Clarity About The Current State To Help You Improve Your Game.'],
            ['icon' => '../front/assets/img/IconImg/access.png', 'title' => 'App Access', 'content' => 'Getting The Necessary Clarity About The Current State To Help You Improve Your Game.'],
            ['icon' => '../front/assets/img/IconImg/video.png', 'title' => 'Video Demonstration', 'content' => 'Getting The Necessary Clarity About The Current State To Help You Improve Your Game.'],
            ['icon' => '../front/assets/img/IconImg/book.png', 'title' => 'Book Quality', 'content' => 'Getting The Necessary Clarity About The Current State To Help You Improve Your Game.'],
            ['icon' => '../front/assets/img/IconImg/ages.png', 'title' => 'Bookmark Fav CoursesCourses For All Ages', 'content' => 'Getting The Necessary Clarity About The Current State To Help You Improve Your Game.'],
        ];
    }
}
