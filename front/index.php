<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Bootstrap demo</title>
    <link
      href="./css/bootstrap.min.css"
      rel="stylesheet"
      crossorigin="anonymous"
    />
    <!-- font awesome cdn link  -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
    />

    <link rel="stylesheet" href="css/main.css" />
  </head>
  <body>
    <header>
      <div class="container">
        <div class="topbar bg-info d-flex justify-content-between">
          <div class="left-bar">
            <ul class="d-flex align-items-center">
              <li>
                <small><b>Mobile.</b> 01778175444</small>
              </li>
              <li>
                <small><b>Email.</b> example@gmail.com</small>
              </li>
            </ul>
          </div>
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="login.html">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Signup</a>
            </li>
            <li class="nav-item">
              <a class="nav-link"
                ><i class="fa-brands fa-facebook-square"></i
              ></a>
            </li>
          </ul>
        </div>
        <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
          <div class="container-fluid">
            <a class="navbar-brand border border-light px-4" href="#"
              ><h2><em>Logo</em></h2></a
            >
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="#"
                    >Home</a
                  >
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item dropdown">
                  <a
                    class="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Service
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Service One</a></li>
                    <li>
                      <a class="dropdown-item" href="#">Service Two</a>
                    </li>
                    <li><hr class="dropdown-divider" /></li>
                    <li>
                      <a class="dropdown-item" href="#">Go to </a>
                    </li>
                  </ul>
                </li>
                <li class="nav-item">
                  <a class="nav-link">Blog</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link">Contact</a>
                </li>
              </ul>
              <form class="d-flex" role="search">
                <input
                  class="form-control me-2"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                <button class="btn btn-outline-success" type="submit">
                  Search
                </button>
              </form>
              <div class="d-flex align-items-center px-4">
                <img src="img/cart.jpg" alt="" width="50" />
                <span class="text-white">10.00</span>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </header>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="slider">
            <div
              id="carouselExampleControls"
              class="carousel slide"
              data-bs-ride="carousel"
            >
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img
                    src="img/slider/slide-1.jpg"
                    class="d-block w-100"
                    alt="..."
                  />
                </div>
                <div class="carousel-item">
                  <img
                    src="img/slider/slider-2.jpg"
                    class="d-block w-100"
                    alt="..."
                  />
                </div>
                <div class="carousel-item">
                  <img
                    src="img/slider/slider-3.jpg"
                    class="d-block w-100"
                    alt="..."
                  />
                </div>
              </div>
              <button
                class="carousel-control-prev"
                type="button"
                data-bs-target="#carouselExampleControls"
                data-bs-slide="prev"
              >
                <span
                  class="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button
                class="carousel-control-next"
                type="button"
                data-bs-target="#carouselExampleControls"
                data-bs-slide="next"
              >
                <span
                  class="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <h1 class="py-5">| Featured Product</h1>

      <div class="row text-center">
        <div class="col-md-4">
          <div
            class="card bg-dark text-white text-center d-flex align-items-center"
            style="height: 100%"
          >
            <img
              src="img/section/left.png"
              class="card-img"
              alt="..."
              height="100%"
            />
            <div class="card-img-overlay">
              <div class="content">
                <h5 class="card-title">SALE</h5>
                <p class="card-text">50% OFF ON NEW ARRIVAL.</p>
                <a href="product-list.html" class="btn btn-danger">SHOP NOW</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-12 pb-4">
              <div class="card bg-dark text-white text-center">
                <img
                  src="img/section/fashion4.jpeg"
                  class="card-img"
                  alt="..."
                  height="300"
                />
                <div class="card-img-overlay">
                  <div class="content">
                    <h5 class="card-title">MEN'S</h5>
                    <p class="card-text">40% OFF ON NEW ARRIVAL.</p>
                    <a href="product-list.html" class="btn btn-danger"
                      >SHOP NOW</a
                    >
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card bg-dark text-white text-center">
                <img
                  src="img/section/fashion.jpg"
                  class="card-img"
                  alt="..."
                  height="300"
                />
                <div class="card-img-overlay">
                  <div class="content">
                    <h5 class="card-title">KID'S</h5>
                    <p class="card-text">50% OFF ON NEW ARRIVAL.</p>
                    <a href="product-list.html" class="btn btn-danger"
                      >SHOP NOW</a
                    >
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card bg-dark text-white text-center">
                <img
                  src="img/section/fashion2.jpg"
                  class="card-img"
                  alt="..."
                  height="300"
                />
                <div class="card-img-overlay">
                  <div class="content">
                    <h5 class="card-title">MEN'S</h5>
                    <p class="card-text">50% OFF ON NEW ARRIVAL.</p>
                    <a href="product-list.html" class="btn btn-danger"
                      >SHOP NOW</a
                    >
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <section class="pt-5 pb-5">
      <div class="container">
        <div class="heading p-3">
          <h2>| NEW ARRIVAL</h2>
        </div>
        <div class="row row-cols-1 row-cols-md-3 g-4 text-center">
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p1.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">32GB Pendrive</h5>
                <p class="card-text">$120.00</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p2.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">Cool Air</h5>
                <p class="card-text">$80.99</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p3.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">Headphone</h5>
                <p class="card-text">$49.99</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p4.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">Mosque Spray</h5>
                <p class="card-text">$199.99</p>
                <a href="#" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p1.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">32GB Pendrive</h5>
                <p class="card-text">$120.00</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p2.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">Cool Air</h5>
                <p class="card-text">$80.99</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p3.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">Headphone</h5>
                <p class="card-text">$49.99</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p4.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">Mosque Spray</h5>
                <p class="card-text">$199.99</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="pt-5 pb-5">
      <div class="container">
        <div class="heading p-3">
          <h2>| MEN'S COLLECTION</h2>
        </div>
        <div class="row row-cols-1 row-cols-md-3 g-4 text-center">
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p1.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">32GB Pendrive</h5>
                <p class="card-text">$120.00</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p2.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">Cool Air</h5>
                <p class="card-text">$80.99</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p3.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">Headphone</h5>
                <p class="card-text">$49.99</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img
                src="img/product/p4.jpg"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title">Mosque Spray</h5>
                <p class="card-text">$199.99</p>
                <a href="product-list.html" class="btn btn-info">Add to Bag</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="subscribe">
      <div class="container py-5">
        <div class="subscribe-content text-center">
          <h2>Get Update Into</h2>
          <p>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellat
            ullam culpa dignissimos neque ipsum alias eum, beatae vel delectus
            commodi
          </p>
          <div class="input-group mb-3">
            <input
              type="email"
              class="form-control"
              placeholder="Type your email..."
              aria-label="Type your email"
              aria-describedby="button-addon2"
              style="padding: 10px"
            />
            <button class="btn" type="button" id="button-addon2">
              Subscribe
            </button>
          </div>
        </div>
      </div>
    </section>
    <footer class="bg-dark">
      <div class="container py-5">
        <div class="row">
          <div class="col-md-4 text-white">
            <h2><em class="border px-4">Logo</em></h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore
              nihil voluptas doloribus debitis veniam quos, accusantium!
            </p>
          </div>
          <div class="col-md-2">
            <h5 class="text-white">Menu Item</h5>
            <ul class="foot-menu">
              <li class="">Privachy & Policy</li>
              <li class="">Terms</li>
              <li class="">FAQ</li>
              <li class="">Carrer</li>
            </ul>
          </div>
          <div class="col-md-2">
            <h5 class="text-white">Menu Item</h5>
            <ul class="foot-menu">
              <li class="">About</li>
              <li class="">Blog</li>
              <li class="">Categories</li>
              <li class="">Brands</li>
            </ul>
          </div>
          <div class="col-md-2">
            <h5 class="text-white">Menu Item</h5>
            <ul class="foot-menu">
              <li class="">An item</li>
              <li class="">A second item</li>
              <li class="">A third item</li>
            </ul>
          </div>
          <div class="col-md-2">
            <h5 class="text-white">Menu Item</h5>
            <ul class="foot-menu" style="background: transparent">
              <li class="">An item</li>
              <li class="">A second item</li>
              <li class="">A third item</li>
              <li class="">A fourth item</li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <script>
      // external js: masonry.pkgd.js

      $(".grid").masonry({
        itemSelector: ".grid-item",
        columnWidth: ".grid-sizer",
        percentPosition: true,
      });
    </script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/fontawesome.min.js"
      integrity="sha512-5qbIAL4qJ/FSsWfIq5Pd0qbqoZpk5NcUVeAAREV2Li4EKzyJDEGlADHhHOSSCw0tHP7z3Q4hNHJXa81P92borQ=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    ></script>
    <script src="js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
  </body>
</html>
