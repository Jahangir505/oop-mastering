<div class="row text-capitalize">
                <?php foreach ($features as $feature): ?>
                <div class="col-md-4 mb-5">
                    <div class="d-inline-block rounded-circle mb-4">
                        <div class="icon-circle icon-circle-lg" style="background-color: #EDEAF6; color: #4E35A3;">
                            <!-- Icon -->
                            <img src="<?= $feature['icon'] ?>" alt="">

                        </div>
                    </div>

                    <h5><?= $feature['title'] ?></h5>
                    <p class="px-lg-4 px-xl-5"><?= $feature['content'] ?></p>
                </div>
                <?php endforeach; ?>
                
            </div>